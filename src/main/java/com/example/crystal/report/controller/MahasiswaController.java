package com.example.crystal.report.controller;

import com.example.crystal.report.model.dto.MahasiswaDTO;
import com.example.crystal.report.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/mahasiswa")
public class MahasiswaController {
    @Autowired
    private MahasiswaService service;

    @GetMapping("/")
    private List<MahasiswaDTO> getAll() {
        return service.getALl();
    }

    @PostMapping("/export")
    private void export() throws IOException {
//        service.ExportWithoutViewer();
    }
}
