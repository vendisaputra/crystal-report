package com.example.crystal.report.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class MahasiswaDTO implements Serializable {
    private static final long serialVersionUID = 5017760951810253839L;

    private Long id;

    private String nim;

    private String nama;
}
