package com.example.crystal.report.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "mahasiswa")
public class Mahasiswa implements Serializable {
    private static final long serialVersionUID = 5690796510942474283L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nim")
    private String nim;

    @Column(name = "nama")
    private String nama;
}
