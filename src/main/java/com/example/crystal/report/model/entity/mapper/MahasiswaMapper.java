package com.example.crystal.report.model.entity.mapper;

import com.example.crystal.report.model.dto.MahasiswaDTO;
import com.example.crystal.report.model.entity.Mahasiswa;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MahasiswaMapper {

    MahasiswaMapper INSTANCE = Mappers.getMapper(MahasiswaMapper.class);

    Mahasiswa toEntity(MahasiswaDTO mahasiswaDTO);

    MahasiswaDTO toDTO(Mahasiswa param);

    List<MahasiswaDTO> toListDTO(List<Mahasiswa> mahasiswas);
}
