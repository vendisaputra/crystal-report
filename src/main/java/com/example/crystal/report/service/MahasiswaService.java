package com.example.crystal.report.service;

import com.example.crystal.report.model.dto.MahasiswaDTO;
import com.example.crystal.report.model.entity.mapper.MahasiswaMapper;
import com.example.crystal.report.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    public List<MahasiswaDTO> getALl(){
        return MahasiswaMapper.INSTANCE.toListDTO(mahasiswaRepository.findAll());
    }
}
